# demo-cicd-express-api-heroku

Gitlab CI/CD Demo with Nodejs Express API, Supertest and Deployment to Heroku

## Heroku:

To create the app on heroku:

```
npm install -g heroku
apt update && apt install git -y
heroku login
heroku create
Creating app... done, -- floating-chamber-36995
https://floating-chamber-36995.herokuapp.com/ | https://git.heroku.com/floating-chamber-36995.git
```

## Resources:

- https://www.codementor.io/wapjude/creating-a-simple-rest-api-with-expressjs-in-5min-bbtmk51mq
