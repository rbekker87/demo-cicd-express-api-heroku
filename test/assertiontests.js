var request = require('supertest');
var test = require('tape');
var app = require('../app.js');

test('A passing test', (assert) => {
  assert.pass('This test will pass.');
  assert.end();
});

test('Assertions with tape.', (assert) => {
  const expected = 'something to test';
  const actual = 'something to test';

  assert.equal(actual, expected,
    'Given two mismatched values, .equal() should produce a nice bug report');
  assert.end();

});

test('GET /random', function(assert) {
    request(app)
      .get('/random')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function(err, res) {
        var expectedusers = ['ruan', 'james', 'frank'];
        var actualusers = res.body;

        assert.error(err, 'No error');
        assert.same(actualusers, expectedusers, 'Retrieve random users');
        assert.end();

    });
});

